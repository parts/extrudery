
// Well, you can go higher, but that would probably be ridiculous
number_of_inputs = 4; // [2,3,4,5,6]
// Pushfit types are e3d threaded pusfit, e3d embedded and printed ring with only the collet put into it. Note, that rendering 4 threaded pushfit sockets borders with impossible, and it's too big, anyway.
pushfit_type = "embeddest"; // [embeddest,embedded,threaded]
join_angle = 15;

// Outer diameter of PTFE liner
liner_outer_diameter = 4;
// Inner diameter of PTFE liner and the diameter of printed filament channel
liner_inner_diameter = 2;

module customizer_multiswitch() {
 multiswitch(
  liner_od = liner_outer_diameter,
  liner_id = liner_inner_diameter,
  angle = join_angle,
  inputs = number_of_inputs,
  pf = pf_byname(stock_pushfits,pushfit_type),
  draft = false,
  print = true,
  debug = 0
 );
}
customizer_multiswitch();
