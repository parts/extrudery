pushfit_metal = [ ["type", "embeddest"],
 ["d",  7],             // outer diameter
 ["id", 5.6],           // inner diameter, the narrow passage
 ["h",  6],             // total height
 ["h_legspace", 3],     // the height of legspace
 ["ch", 0.4]            // inlet chamfer
];
pushfit_embeddest = [ ["type", "embeddest"],
 ["d",  8],             // outer diameter of legspace
 ["id", 6],             // inner diameter, the narrow passage
 ["h",  7],             // total height
 ["h_legspace", 3.2],   // the height of legspace
 ["ch", 1]             // inlet chamfer
];
pushfit_embedded = [ ["type", "embedded"],
 ["d", 12],             // outer diameter
 ["h", 7 ],             // total height
 ["h_ring", 4.7],       // height of embedded pushfit ring
 ["d_insert", 8],       // diameter of pushfit insert legs hole
];
pushfit_threaded = [ ["type", "threaded"],
 ["h", 10],             // total height
 ["d", 25.4*3/8 ],      // the diameter to return
 ["d_tolerance", 0.5],  // the diameter tolerance
 ["tpi", 28]           // threads per inch
];

stock_pushfits = [
 ["threaded",  pushfit_threaded],
 ["embedded",  pushfit_embedded],
 ["embeddest", pushfit_embeddest]
];
