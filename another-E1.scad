use <another.scad>;

what="body";
vitamins=false;

rotate([180,0,0])
the_extruder(what=what, vitamins=vitamins,

 left=false,
 pulley_d=11.5, pulley_elevation=1,
 teeth_elevation=7.5,
 bore_l=17.6
);
