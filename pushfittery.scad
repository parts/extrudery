layer_height=.2; epsilon=.01;
draft = true;

use <threads.scad>;

function pf_(d,k) = d[search([k],d)[0]][1];
function pf_d(pf) = pf_(pf,"d");
function pf_h(pf) = pf_(pf,"h");
module pushfit(pf,draft=draft) {
 fnd = 2*PI;
 type = pf_(pf,"type");
 h = pf_h(pf);
 if(type=="threaded") {
  minch = 25.4;
  d = (pf_d(pf) + pf_(pf,"d_tolerance"))/minch;
  tpi = pf_(pf,"tpi");
  if(draft) cylinder(d=d*minch,h=h+1);
  else english_thread(diameter=d,threads_per_inch=tpi,length=(h+1)/minch,internal=true);
  slitl = d*minch+layer_height;
  slitw = 0.8*d*minch/2;
  echo(slitw,slitl);
  translate([-slitw/2,-slitl/2,0]) cube([slitw,slitl,h+1]);
 }else if(type=="embedded") {
  d = pf_d(pf);
  h_ring = pf_(pf,"h_ring");
  d_insert = pf_(pf,"d_insert");
  $fn = d*fnd;
  translate([0,0,h-h_ring]) cylinder(d=d,h=h_ring+1);
  cylinder(d=d_insert,h=h);
 }else if(type=="embeddest") {
  d = pf_d(pf);
  id = pf_(pf,"id");
  h_legspace = pf_(pf,"h_legspace");
  ch = pf_(pf,"ch");
  $fn = d*fnd;
  cylinder(d=id,h=h+1);
  cylinder(d=d,h=h_legspace);
  dd = (d-id)/2;
  translate([0,0,h_legspace-epsilon])
  cylinder(d1=d,d2=id-2*epsilon,h=dd+epsilon);
  translate([0,0,h-ch-epsilon])
  cylinder(d1=id-2*epsilon,d2=id+2*ch+2,h=ch+epsilon+1);
 }
}//pushfit module

function pf_byname(d,n) = d[search([n],d)[0]][1];
