layer_height=.2; extrusion_width=.5;
epsilon=.01;
draft = true;

use <pushfittery.scad>;
include <pushfit_data.scad>;

module multiswitch(
 liner_od = 4, liner_id = 2,
 angle = 15,    // to the vertical (output) axis
 inputs = 4,
 minshell = 2*extrusion_width,
 shell = 5*extrusion_width,
 pf = pushfit_embeddest,

 debug = 0,     // how many inputs -1 the debug cutout spans
 draft = draft,
 print = true,

 liner_d_tolerance=.2
) {
 fnd = 4*PI; fnr = 2*fnd;
 
 pushfit_d = pf_d(pf);
 pushfit_h = pf_h(pf);
 
 angular_step = 360/inputs;
 lod = liner_od+liner_d_tolerance; // effective liner diameter
 
 sinsin = sin(angle)*sin(angular_step/2);
 function l_to(d) = d*cos(asin(sinsin))/sinsin;
 l_output = lod;
 l_input = l_to((pushfit_d+minshell)/2);
 l_fork = l_to(liner_id/2);
 l_narrow = l_to(lod/2+minshell);
 
 module forinputs() {
  for(zr=[0:angular_step:359]) rotate([0,0,zr]) rotate([0,angle,0]) children();
 }//forinputs module
 module foroutput() {
  rotate([180,0,0]) children();
 }
 
 module laydown() {
  r = pushfit_d/2+shell;
  h_bottom = l_output+pushfit_h;
  /* The top point on the cylinder that will touch the bed */
  x0 = r*cos(angular_step/2);
  y0 = r*sin(angular_step/2);
  z0 = l_input+pushfit_h;
  /* The same point after rotation by "angle" around Y axis */
  x1 = z0*sin(angle)+x0*cos(angle);
  y1 = y0;
  z1 = z0*cos(angle)-x0*sin(angle);
  ax1 = atan(y1/x1);
  /* And its x-coordinate after final "angular_step/2" Z-rotation */
  ax2 = ax1-angular_step/2;
  x2 = x1*cos(ax2)/cos(ax1);
  laydown_angle = atan((x2-r)/(z1+h_bottom));
  rotate([90-laydown_angle,0,0])
   translate([0,r,h_bottom])
    rotate([0,0,angular_step/2-90])
     children();
 }
 module finalize() {
  if(print) laydown() children();
  else children();
 }

 finalize() difference() {
  hull() {
   forinputs()
    translate([0,0,l_input+pushfit_h]) mirror([0,0,1])
    cylinder(d=pushfit_d+shell*2,h=epsilon,$fn=pushfit_d*fnd);
   foroutput()
    translate([0,0,l_output+pushfit_h]) {
     cylinder(d=pushfit_d+shell*2,h=epsilon,$fn=pushfit_d*fnd);
    }
  }
  forinputs() {
   translate([0,0,l_input]) pushfit(pf,draft=draft);
   translate([0,0,l_narrow]) {
    cylinder(d=lod,h=l_input+1-l_narrow,$fn=lod*fnd);
    mirror([0,0,1]) translate([0,0,-epsilon])
    cylinder(d1=(liner_id+lod)/2,d2=liner_id,h=liner_id,$fn=lod*fnd);
   }
   cylinder(d=liner_id,h=l_input+epsilon,$fn=liner_id*fnd);
  }
  foroutput() {
   translate([0,0,l_output]) pushfit(pf,draft=draft);
   cylinder(d=lod,h=l_output+1,$fn=lod*fnd);
  }
  hull() {
   forinputs()
    translate([0,0,l_fork]) cylinder(d=liner_id,h=epsilon,$fn=liner_id*fnd);
   foroutput()
    cylinder(d=liner_id,h=epsilon,$fn=liner_id*fnd);
  }
  if(debug) {
   translate([0,0,-20/*TODO:*/])
   rotate_extrude(angle=angular_step*debug)
   square([50,100]/*TODO:*/);
  }
 }

}


//CUSTOMIZERCUTOFF
multiswitch(debug=1,print=false,inputs=2);
