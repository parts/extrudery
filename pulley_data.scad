pulley_e3d = [
 ["d", 11.5 ],			// outer diameter
 ["h", 11 ],			// pulley height
 ["g", 8.25-3 - 11.5/2 ],	// depth of U-groove, filament offset from the surface
 ["fe", 7.5 ],			// filament elevation from the bottom
 ["sse", 2.5 ],			// set screw elevation
 ["ssd", 3.1 ],			// set screw diameter
 ["ssrc", 2 ],			// set screw radial clearance
];

