draft=true;
layer_height=0.2; extrusion_width=0.4;
epsilon=0.01;
$fs=0.0125;

use <pushfittery.scad>;
include <pushfit_data.scad>;
include <pulley_data.scad>;

module the_extruder(
 // motor properties
 gearbox_d = 36,
 mount_d = 28,					// the distance between opposite mounting holes
 mounthole_depth = 5,
 mounthole_min_depth = 3, mounthole_max_depth = 4,
 protrusion_d = 22, protrusion_h = 2.2,		// the dimensions of the protrusion on top of gearbox
 bore_d = 8, bore_l = 17.6,
 bore_dd = 7,
 // pulley properties
 pulley = pulley_e3d,
 pulley_elevation = 1,				// pulley elevation above the protrusion
 // idler properties
 idler_d = 10, idler_h = 4, idler_id = 3,	// idler dimensions: outer and inner diameters and height
 // spring properties
 spring_d = 10, spring_lc = 9.6,		// spring diameter and compressed length
 // filament path properties
 filament_d = 1.75,
 filament_path_d = 2,
 filament_guide_d = 4,				// PTFE filament guide diameter
 // knob properties
 knob_h = 10,
 knob_indent_d = 4,
 knob_indents = 12,
 knob_v_clearance = 1,
 // spring tensioner
 st_nut_d = 9, st_nut_h = 4,
 st_screw_d = 5, st_screw_l = 16,
 st_thickshell = 8*extrusion_width, st_thinshell = 2*extrusion_width,
 st_split_w = 4*extrusion_width,

 // screw it
 mount_screw_d = 3.1, mount_screw_l = 20,
 mount_screwhead_d=6, mount_screwhead_h=3,
 idler_screw_d = 3,
 idler_screwhead_d=6, idler_screwhead_h=3,

 pf = pushfit_metal,
 pf_shell = max(3*layer_height,3*extrusion_width),

 // empty spaces
 idler_travel = 1.75*2,				// how far should idler travel when pressed
 idler_clearance=.5,
 pulley_small_d_clearance=.5,
 lever_v_clearance=.7,				// vertical clearance for the lever
 spring_d_clearance=1,
 protrusion_tolerance_h=.5,			// horizontal tolerance for the motor protrusion
 protrusion_tolerance_v=.5,			// vertical tolerance for the motor protrusion
 mount_screw_d_tolerance=.4,
 idler_v_tolerance=.5,
 knob_bore_d_tolerance=.6,
 st_nut_h_tolerance=.2,
 st_nut_d_tolerance=.2,/* TODO: make it st_nut_w_tolerance */
 st_screw_d_tolerance=.5,
 st_split_w_tolerance = .3,

 min_z_shell = 4*layer_height,                  // the very minimal shell thickness along z-axis.
 min_xy_shell = 2*extrusion_width,

 body_label = "another", body_label_thickness = layer_height,
 knob_label = "another", knob_label_thickness = 2*layer_height, knob_label_size=undef,

 what="lever",  // lever|body|knob|springpad|*
 left=false,
 vitamins = true,
 bridges = true,
 debug = false,
) {
 fnd = 2*PI; fnr = 2*fnd;
 function lu(m,k) = m[search([k],m)[0]][1];

 pulley_d = lu(pulley,"d");
 pulley_h = lu(pulley,"h");
 teeth_elevation = lu(pulley,"fe");
 filament_offset = lu(pulley,"g")+filament_d/2;
 pulley_d_clearance = lu(pulley,"ssrc")*2;

 sp_ch = st_split_w;

 idler_filament_offset = filament_path_d/2;
 lever_shell = mount_screwhead_h+0.5;
 lever_thickness=max(spring_d+spring_d_clearance+layer_height*8,idler_h+idler_v_tolerance+2*lever_shell);
 lsd = min(gearbox_d-mount_d,idler_d-idler_clearance*2); // the diameter of lever thingie
 longwing=(mount_d+mount_screw_d+mount_screw_d_tolerance+spring_d+spring_d_clearance)/2+min_xy_shell;
 shortwing = let(a=mount_d/2,c=(pulley_d+idler_d)/2/sqrt(2))
             sqrt(c*c+pow(a-c,2));
 longwing_travel = idler_travel*longwing/shortwing;
 h_ = (pulley_d+idler_d)/(2*sqrt(2));
 ri = sqrt( pow(h_,2) + pow(mount_d/2-h_,2) );
 spring_dl = idler_travel*longwing/ri;
 // finger and spring support width
 fsw = gearbox_d-(gearbox_d-mount_d)/2-max(spring_lc,longwing_travel+(gearbox_d-mount_d)/2);

 mount_min_depth = mounthole_min_depth ? mounthole_min_depth : mounthole_depth-1;
 mount_max_depth = mounthole_max_depth ? mounthole_max_depth : mounthole_depth+1;
 filament_elevation=protrusion_h+pulley_elevation+teeth_elevation;
 ls_z = filament_elevation; // leverspace mid-z
 ls_h = lever_thickness+lever_v_clearance; // leverspace height
 mount_depth = mount_screw_l - (ls_z+ls_h/2+min_z_shell);
 body_h = max(protrusion_h+bore_l,mount_screw_l-mount_depth+mount_screwhead_h+min_z_shell,ls_z*2);
 echo("mount depth",mount_depth);

 pf_offset = mount_d/sqrt(2)/2+mount_screw_d/2+min_xy_shell;

 module teardrop(r,d,h,center=false,angle=45) {
  dd = d ? d : (2*r);
  $fn = dd*fnd;
  cylinder(d=dd,h=h,center=center);
  if(angle>0) translate([0,0,center?-h/2:0])
   rotate([0,0,angle])
   cube(size=[d/2,d/2,h]);
 }

 module mirrorleft() {
  mirror([left?0:1,0,0]) children();
 }
 module place_idler() {
  rotate([0,0,45])
  translate([(pulley_d+idler_d)/2,0,0])
  children();
 }
 module finger_indent(d=lever_thickness,depth/*=1*/,r/*=15*/) {
  if(depth) {
   hh = (-4*pow(depth,2)+pow(d,2))/(8*depth);
   rr = depth+hh;
   translate([0,0,hh]) sphere(r=rr,$fn=fnr*rr);
  }else if(r) {
   hh=sqrt(pow(r,2)-pow(d,2)/4);
   translate([0,0,hh]) sphere(r=r,$fn=fnr*r);
  }
 }

 % if(vitamins) mirrorleft() {
  translate([0,0,-epsilon]) mirror([0,0,1]) cylinder(d=gearbox_d,h=1,$fn=60);
  for(zr=[0:90:359]) rotate([0,0,zr]) translate([mount_d/2,0,0])
   cylinder(d=mount_screw_d,h=20,$fn=30);
  translate([0,0,-epsilon]) cylinder(d=protrusion_d,h=protrusion_h,$fn=30);
  translate([0,0,protrusion_h]) {
   difference() {
    cylinder(d=bore_d,h=bore_l,$fn=30);
    translate([-bore_d/2-1,bore_dd-bore_d/2,-1])
    cube([bore_d+2,bore_d/2,bore_l+2]);
   }
   translate([0,0,pulley_elevation]) {
    cylinder(d=pulley_d,h=pulley_h,$fn=30);
    translate([0,0,teeth_elevation]) {
     place_idler() {
      cylinder(d=idler_d,h=idler_h,center=true,$fn=30);
      cylinder(d=idler_screw_d,h=lever_thickness+2,center=true,$fn=30);
     }//place idler
     // filament path
     rotate([0,0,45]) {
      translate([pulley_d/2-idler_filament_offset,0,0]) rotate([90,0,0]) {
       cylinder(d=filament_d,h=gearbox_d,center=false,$fn=15);
      }//rotate translate
      translate([pulley_d/2+filament_offset,0,0]) rotate([-90,0,0]) {
       cylinder(d=filament_d,h=gearbox_d,center=false,$fn=15);
       translate([0,0,pf_offset])
       pushfit(pf);
      }//rotate translate
     }//rotate
    }//translate teeth
   }//translate pulley
  }//translate protrusion
  // spring tensioner screw
  translate([-gearbox_d/2,-longwing,ls_z])
  rotate([0,90,0])
  cylinder(d=st_screw_d,h=st_screw_l);
 }//mirrorleft if(vitamins)

 module lever() {
  translate([0,0,protrusion_h+pulley_elevation+teeth_elevation]) {
   difference() {
    union() {
     hull() {
      place_idler()
      cylinder(d=lsd,h=lever_thickness,center=true,$fn=60);
      translate([mount_d/2,0,0])
      cylinder(d=lsd,h=lever_thickness,center=true,$fn=60);
     }//hull
     hull() {
      translate([mount_d/2,0,0])
      cylinder(d=lsd,h=lever_thickness,center=true,$fn=60);
      translate([mount_d/2,-longwing,0]) rotate([0,90,0])
      cylinder(d=lever_thickness,h=lsd,center=true,$fn=60);
     }//hull
    }//union
 
    // filament path
    place_idler()
    translate([-idler_d/2-idler_filament_offset,0,0])
    rotate([90,0,0]) {
     mirror([0,1,0])
     teardrop(d=filament_path_d,h=3*gearbox_d,center=true);
     translate([0,0,
      sqrt(2)*(mount_d-pulley_d+lsd)/2
     ]) {
      cylinder(d1=filament_path_d,d2=2*filament_path_d,
               h=filament_path_d,$fn=2*filament_path_d*fnd);
      translate([0,0,filament_path_d-epsilon])
      cylinder(d=2*filament_path_d,h=lsd*sqrt(2),$fn=2*filament_path_d*fnd);
     }//translate
    }//rotate translate place_idler

    // idler space and mounting hole
    place_idler() {
     cylinder(d=idler_d+idler_clearance*2,h=idler_h+idler_v_tolerance,center=true,$fn=60);
     cylinder(d=idler_screw_d,h=lever_thickness+2,center=true,$fn=30);
     translate([0,0,lever_thickness/2-idler_screwhead_h])
     cylinder(d=idler_screwhead_d,h=idler_screwhead_h+1,$fn=fnd*idler_screwhead_d);
    }
    // mounting screw hole
    translate([mount_d/2,0,0])
    cylinder(d=mount_screw_d+mount_screw_d_tolerance,h=lever_thickness+2,center=true,$fn=fnd*mount_screw_d);
    
    // lever end
    translate([mount_d/2,0,0]) rotate([0,90,0]) {
     translate([0,-longwing,lsd/2]) finger_indent(d=lever_thickness-1,r=15);
     translate([0,-longwing,0])
     mirror([0,0,1])
     difference() {
      cylinder(d=spring_d+spring_d_clearance,h=lsd,$fn=fnd*(spring_d+spring_d_clearance));
      sphere(d=spring_d*3/4,$fn=fnd*spring_d*3/4);
     }
    }//rotate-translate
   }//difference
   // bridging patch
   if(bridges) place_idler()
   translate([0,0,lever_thickness/2-mount_screwhead_h])
   mirror([0,0,1])
   cylinder(d=mount_screwhead_d,h=layer_height);
  }//translate
 }//lever module
 
 module body() {
  difference() {
   union() {
    cylinder(d=gearbox_d,h=body_h,$fn=fnd*gearbox_d);
    // spring tensioner part
    translate([-gearbox_d/2,0,0]) hull() {
     translate([0,-longwing,ls_z])
     rotate([0,90,0])
     cylinder(d=lever_thickness,h=fsw,$fn=fnd*lever_thickness);
     hh=body_h-ls_z;
     translate([0,0,ls_z])
     mirror([0,1,0]) cube(size=[fsw,longwing+(st_split_w+st_split_w_tolerance)/2+st_thickshell,body_h-ls_z]);
     hhh=ls_z;
     translate([0,0,0])
     mirror([0,1,0]) cube(size=[fsw,longwing-hhh+lever_thickness/sqrt(2),hhh+lever_thickness/2]);
    }//hull translate
    
    // pushfit bracket
    translate([0,0,filament_elevation])
    rotate([0,0,45]) translate([pulley_d/2+filament_offset,0,0])
    rotate([-90,0,0])
    translate([0,0,pf_offset-gearbox_d/2/*TODO:*/]) {
     pfbl = pf_h(pf)+gearbox_d/2; //TODO:
     pfbd = pf_d(pf)+2*pf_shell;
     hull() {
      cylinder(d=pfbd,h=pfbl,$fn=pfbd*fnd);
      translate([-epsilon/2,0,0]) {
       cube(size=[epsilon,filament_elevation,pfbl-filament_elevation+pfbd/2]);
       mirror([0,1,0])
       cube(size=[epsilon,body_h-filament_elevation,pfbl-body_h+filament_elevation+pfbd/2]);
      }//translate
     }//hull
    }//translate rotate translate rotate translate
   }//union (first child of difference)

   // protrusion
   translate([0,0,-1])
   cylinder(d=protrusion_d+protrusion_tolerance_h,h=protrusion_h+protrusion_tolerance_v+1,$fn=fnd*(protrusion_d+protrusion_tolerance_h));
   // mount screw holes
   for(zr=[0:90:359]) rotate([0,0,zr]) translate([mount_d/2,0,0]) {
    translate([0,0,mount_screw_l-mount_depth-(bridges?layer_height:-epsilon)])
    mirror([0,0,1])
    cylinder(d=mount_screw_d+mount_screw_d_tolerance,
             h=mount_screw_l-mount_depth+1,
	     $fn=fnd*(mount_screw_d+mount_screw_d_tolerance));
    translate([0,0,mount_screw_l-mount_depth])
    cylinder(d=mount_screwhead_d,h=body_h+1,$fn=fnd*mount_screwhead_d);
   }//for
   // pushfit
   translate([0,0,filament_elevation])
   rotate([0,0,45]) translate([pulley_d/2+filament_offset,0,0])
   rotate([-90,0,0])
   translate([0,0,pf_offset])
   rotate([0,0,180]) {
    pushfit(pf);
    mirror([0,0,1]) {
     fh = 1.5* (filament_guide_d-filament_path_d)/2;
     fo = pf_offset -filament_path_d /*(idler_d+idler_clearance)/2*/-fh;
     translate([0,0,-1]) teardrop(d=filament_guide_d,h=fo/2,angle=225);
     translate([0,0,-1]) teardrop(d=filament_path_d,h=gearbox_d/2+1,angle=-225);
     translate([0,0,fo+epsilon])
     cylinder(d1=filament_path_d,d2=filament_guide_d,h=fh,$fn=fnd*filament_guide_d);
    }//mirror
   }//rotate translate rotate translate rotate translate
   // pulley
   cylinder(d=pulley_d+pulley_small_d_clearance,h=body_h+1,$fn=fnd*(pulley_d+pulley_d_clearance));
   cylinder(d=pulley_d+pulley_d_clearance,ls_z-max(idler_h,filament_guide_d)/2,$fn=fnd*(pulley_d+pulley_d_clearance));

   translate([0,0,ls_z-ls_h/2]) {
    // idler travel
    translate([mount_d/2,0,0])
    rotate([0,0,45])
    rotate_extrude(angle=90,$fn=fnr*(shortwing+(idler_d+idler_clearance)/2))
    square([shortwing+(idler_d+idler_clearance)/2,ls_h]);
    // idler itself
    place_idler()
    cylinder(d=idler_d+idler_clearance,h=ls_h,$fn=fnd*(idler_d+idler_clearance));
    // lever space
    rotate([0,0,45])
    rotate_extrude(angle=-135+max(atan(2*fsw/gearbox_d-1),atan(gearbox_d/mount_d-1)))
    square([gearbox_d/2+1,ls_h]);
   }

   // back side cutout
   // XXX: this is somewhat ugly and potentially bugged,
   //      but I want to sleep.
   a0_ = atan(
    (pf_offset+pf_h(pf)+gearbox_d/2)
    /
    (
     pulley_d/2+filament_offset-
     (pf_d(pf)+2*pf_shell)/2/cos(30)
    )
   );
   a0 = -135 + ( (a0_>0) ? a0_ : 180+a0_ );
   a1 = atan((longwing-lever_thickness/2)/(gearbox_d/2));
   translate([0,0,ls_z]) difference() {
    rotate([0,0,-180+a0])
    rotate_extrude(angle=a1-a0)
    translate([0,-ls_h/2])
    square([sqrt(pow(gearbox_d/2,2)+pow(longwing-lever_thickness/2,2))+1,
           ls_h]);
    translate([-mount_d/2,0,0])
    cylinder(d=gearbox_d-mount_d,h=ls_h+2,center=true,$fn=fnd*(gearbox_d-mount_d));
   }//difference translate
   
   // spring tensioner
   translate([-gearbox_d/2,-longwing,ls_z]) rotate([0,-90,0]) {
    mirror([0,0,1]) {
     translate([0,0,st_thickshell])
     hull() for(o=[0,spring_d]) translate([0,-o,0])
     rotate([0,0,30])
     cylinder(d=st_nut_d+st_nut_d_tolerance,h=st_nut_h+st_nut_h_tolerance,$fn=6);
     translate([0,0,-1]) cylinder(d=st_screw_d+st_screw_d_tolerance,h=fsw+2,$fn=fnd*(st_screw_d+st_screw_d_tolerance));
     bigd = spring_d+spring_d_clearance;
     hf = (bigd-st_screw_d-st_screw_d_tolerance)/2;
     translate([0,0,st_thickshell+st_nut_h+st_nut_h_tolerance+st_thinshell-epsilon]) {
      cylinder(d1=st_screw_d+st_screw_d_tolerance,d2=bigd,
       h=hf+epsilon, $fn=fnd*bigd);
      translate([0,-(st_split_w+st_split_w_tolerance)/2,0]) hull() {
       cube(size=[body_h-ls_z-st_w+st_split_w_tolerance,st_split_w+st_split_w_tolerance,fsw+1]);
       translate([0,0,st_w+1])
       cube(size=[body_h-ls_z+1,st_split_w+st_split_w_tolerance,fsw+1]);
      }
     }//translate
     translate([0,0,st_thickshell+st_nut_h+st_nut_h_tolerance+st_thinshell+hf-epsilon])
     cylinder(d=bigd,h=fsw+1,$fn=fnd*bigd);
    }//mirror
   }//translate

   // label
   body_label();
   // scale
   body_scale();

  }//difference

 }//body module
 module body_scale() {
  scale_thickness = 2*layer_height;
  nscales = 5;
  x1 = -gearbox_d/2+fsw-st_h+st_w+st_bump*2-st_bump_tip/2;
  x0 = -gearbox_d/2+st_thickshell+st_nut_h+st_nut_h_tolerance+st_thinshell+st_split_w+st_split_w_tolerance+st_bump_tip/2;
  w00 = st_split_w+2*(st_bump-st_bump_tip);
  w0 = w00+2*2*extrusion_width;
  w1 = 2*(longwing-gearbox_d/2-extrusion_width);
  translate([0,-longwing,body_h-scale_thickness])
  difference() {
   for(x=[x0:(x1-x0)/(nscales-1):x1]) let(w=w0+(w1-w0)*(x-x0)/(x1-x0))
    translate([x-st_bump_tip/2,-w/2,0])
    cube(size=[st_bump_tip,w,scale_thickness+epsilon]);
   let(w=w00)
   translate([-gearbox_d/2,-w/2,-1])
   cube(size=[fsw,w,scale_thickness+2]);
  }
 }
 module body_label() {
  if(body_label) {
   label=body_label;
   label_thickness=body_label_thickness;
   label_len = len(label);
   labelspace = [gearbox_d/2-mount_screwhead_d/2-min_xy_shell,longwing-(mount_screwhead_d-st_split_w-st_split_w_tolerance)/2-2*min_xy_shell];
   translate([0,0,body_h-label_thickness])
   linear_extrude(height=label_thickness+epsilon,convexity=64)
   translate([-mount_screwhead_d/2-min_xy_shell-labelspace.x/2,-longwing+(st_split_w+st_split_w_tolerance)/2+min_xy_shell+labelspace.y/2])
   mirror([left?0:1,0])
   text(text=label,size=labelspace.x/label_len,halign="center",valign="center",font="Arial Black");
  }else sphere(d=epsilon); // to avoid makefile breakage
 }

 st_bump = 4*extrusion_width;
 st_bump_tip = 2*extrusion_width;
 st_smalld = st_screw_d+st_screw_d_tolerance+min_xy_shell;
 st_bigd = spring_d+spring_d_clearance/2;
 st_hf = (st_bigd-st_smalld)/2;
 st_h = max(st_hf*3,fsw-st_screw_l+st_hf);
 st_w = sp_ch;
 module springpad() {
  smalld=st_smalld;
  bigd = st_bigd;
  hf = st_hf;
  h = st_h;
  translate([-gearbox_d/2+fsw-h,-longwing,filament_elevation])
  rotate([0,90,0]) difference() {
   union() {
    cylinder(d1=smalld,d2=bigd,h=hf,$fn=fnd*bigd);
    translate([0,0,hf-epsilon])
    cylinder(d=bigd,h=h-hf,$fn=fnd*bigd);
    translate([0,0,h])
    sphere(d=spring_d*3/4,$fn=fnd*spring_d*3/4);
    l = body_h-ls_z;
    w = st_w;
    translate([0,-st_split_w/2,0]) mirror([1,0,0]) {
     hull()  {
      translate([0,0,w+st_bump])
      cube(size=[l+st_bump,st_split_w,w]);
      cube(size=[l-w,st_split_w,2*w]);
     }
     translate([l,0,w+st_bump]) hull() {
      cube(size=[st_split_w,st_split_w,st_bump]);
      translate([0,-st_bump+st_bump_tip,st_bump]) mirror([0,0,1])
      cube(size=[st_split_w,st_split_w+2*(st_bump-st_bump_tip),st_bump_tip]);
     }
    }
   }
   translate([0,0,-epsilon])
   cylinder(d=st_screw_d+st_screw_d_tolerance,
    h=min(st_screw_l,fsw-hf)-fsw+h+2*epsilon,$fn=fnd*(st_screw_d+st_screw_d_tolerance));
  }
 }

 module knob() {
  ch = knob_indent_d/2;
  knob_bore_l = body_h-protrusion_h-pulley_elevation-pulley_h-knob_v_clearance;
  translate([0,0,body_h+epsilon]) {
   mirror([0,0,1]) translate([0,0,-epsilon]) {
    difference() {
     cylinder(d=pulley_d,h=knob_bore_l,$fn=fnd*pulley_d);
     translate([0,0,-1]) difference() {
      cylinder(d=bore_d+knob_bore_d_tolerance,h=knob_bore_l+2);
      translate([-bore_d/2-1,bore_dd-bore_d/2+knob_bore_d_tolerance/2,-1])
      cube([bore_d+2,bore_d/2,knob_bore_l+4]);
     }
    }
   }
   difference() {
    hull() {
     cylinder(d=gearbox_d-2*ch,h=knob_h,$fn=fnd*gearbox_d);
     translate([0,0,ch])
     cylinder(d=gearbox_d,h=knob_h-2*ch,$fn=fnd*gearbox_d);
    }
    for(a=[0:360/knob_indents:359]) rotate([0,0,a])
     translate([0,gearbox_d/2,-1])
     cylinder(d=knob_indent_d,h=knob_h+2,$fn=fnd*knob_indent_d);
    translate([0,0,-body_h-epsilon]) knob_label();
   }
  }
 }//knob module
 module knob_label() {
  if(knob_label) {
   ll = len(knob_label);
   lsl = gearbox_d-knob_indent_d;
   ts = knob_label_size?knob_label_size:min(lsl/ll,gearbox_d/4);
   translate([0,0,body_h+epsilon+knob_h-knob_label_thickness])
   linear_extrude(height=knob_label_thickness+epsilon,convexity=64)
   mirror([left?0:1,0])
   text(text=knob_label,size=ts,halign="center",valign="center",font="Arial Black");
  }else sphere(d=epsilon); // to avoid makefile breakage
 }

 module debug() {
  difference() {
   children();
   if(debug) {
    // spring tensioner debug cutout
    translate([-gearbox_d/2,-longwing,ls_z]) rotate([0,-90,0])
    translate([0,-lever_thickness/2-1,-gearbox_d-1])
    cube(size=[body_h-ls_z+st_bump+1,lever_thickness/2+1,gearbox_d+2]);
    // lever axis debug cutout
    translate([mount_d/2,0,-1])
    rotate([0,0,-45])
    cube(size=[gearbox_d-mount_d,gearbox_d-mount_d,body_h+knob_h+2]);
    // idler debug cutout
    translate([0,0,ls_z])
    place_idler()
    rotate([0,0,-45])
    translate([0,0,-lever_thickness/2-epsilon])
    cube(size=[lsd*sqrt(2)/2+1,lsd*sqrt(2)/2+1,
               lever_thickness+2*epsilon]);
    // pushfit debug cutout
    place_idler()
    translate([-idler_d/2+filament_offset,0,ls_z])
    cube(size=[gearbox_d/2,gearbox_d/2+pf_h(pf),body_h-ls_z+knob_h+1]);
   }//if debug
  }//difference
 }

 mirrorleft()
 debug()
 if(what=="lever") color("green",0.7) lever();
 else if(what=="body") color("yellow",0.7) body();
 else if(what=="body.label") color("brown",0.7) body_label();
 else if(what=="body.scale") color("brown",0.7) body_scale();
 else if(what=="knob") color("red",0.7) knob();
 else if(what=="knob.label") color("brown",0.7) knob_label();
 else if(what=="springpad") color("blue",0.7) springpad();
 else if(what=="both") {
  color("green",0.7) lever();
  color("yellow",0.7) body();
  color("brown",0.7) body_label();
  color("brown",0.7) body_scale();
 }else{
  color("green",0.7) lever();
  color("yellow",0.7) body();
  color("brown",0.7) body_label();
  color("brown",0.7) body_scale();
  color("red",0.7) knob();
  color("brown",0.7) knob_label();
  color("blue",0.7) springpad();
 }
}

the_extruder(what="*",left=false,debug=true);
