use <another.scad>;

what="body";
vitamins=false;

rotate([180,0,0])
the_extruder(what=what, vitamins=vitamins,

 left=true,
 pulley_d=12.65, pulley_elevation=1,
 teeth_elevation = 7.88,
 bore_l = 20
);
