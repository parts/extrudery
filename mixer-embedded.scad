extrusion_width=.4;

use <mixer.scad>;

the_mixer(
 pushfit_type="embedded",
 pushfit_d = 12,
 pushfit_h = 7,
 interpushfit = extrusion_width
);
