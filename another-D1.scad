use <another.scad>;

what="*";
vitamins=false;

rotate([180,0,0])
the_extruder(what=what, vitamins=vitamins,

 left=true,
 pulley_d=12, pulley_elevation=2,
 teeth_elevation = 7.5,
 bore_l = 17,
 mounthole_depth = 10,
 idler_d = 10, idler_h = 4, idler_id =3,
 knob_bore_l = 8,
 spring_lc = 8,

 knob_bore_d_tolerance=.8
);
