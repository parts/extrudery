// The diameter of the inner collet
inner_diameter = 6;
// The diameter of the inner collet top lip
outer_diameter = 10;
// Thickness
thickness = 1.5;

/* [Hidden] */
layer_height=0.15; extrusion_width=0.4;
epsilon=.01;

module unbacklash(
 id=inner_diameter,
 od=outer_diameter,
 h=thickness,
 alpha=10,
 overlap=0.8,
 split=2*extrusion_width,
) {
 fnd=PI*4; fnr=2*fnd;
 sd = (od-id)/2+overlap;
 s = sd;
 r = sd/2;
 H=3*h;
 % translate([0,0,-h]) cylinder(d=id,h=3*h,$fn=id*fnd);
 difference() {
  union() {
   translate([-od/2,0,0])
   cube(size=[od,od/2+s-r,h]);
   hull() for(sx=[-1,1])
    translate([sx*(od/2-r),od/2+s-r,0])
    cylinder(r=r,h=H,$fn=r*fnr);
   for(sx=[-1,1])
    hull() for(y=[0,-od/2+sd/2])
     translate([sx*(od/2-sd/2),y,0])
     cylinder(d=sd,h=h,$fn=sd*fnd);
   * translate([-od/2,od/2,0])
   cube(size=[od,r,H]);
  }
  translate([0,0,-1]) cylinder(d=id,h=h+2,$fn=od*fnd);
  translate([-id/2+overlap,-od/2-1,-1]) cube(size=[id-2*overlap,od/2+1,h+2]);
  translate([0,0,h])
  rotate([alpha,0,0])
  translate([-od/2-1,-od,0])
  cube(size=[od+2,od,h]);
  hull() for(y=[0,od/2-split/2])
   translate([0,y,-1])
   cylinder(d=split,h=h+2,$fn=split*fnd);
  translate([0,od/2-split,-1]) cylinder(r=split,h=h+2,$fn=split*fnr);
 }
}

unbacklash();
