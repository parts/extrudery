-include Makefile.local

OPENSCAD_APP?=/Applications/OpenSCAD.app
OPENSCAD_BIN?=${OPENSCAD_APP}/Contents/MacOS/OpenSCAD
OPENSCAD_FLAGS=-D draft=false -D debug=false
SED?=sed
ECHO?=/bin/echo
AWK?=awk

default:
	@echo "And?"

clean:
	rm -f *.stl *.gcode

stl-another-%:
	$(MAKE) another-$*-{body,body-label,body-scale,lever,knob,knob-label,springpad}.stl

another-%-body.stl: another-%.scad another.scad
	$(OPENSCAD_BIN) $(OPENSCAD_FLAGS) -D 'what="body"' -o "$@" "$<"
another-%-body-label.stl: another-%.scad another.scad
	$(OPENSCAD_BIN) $(OPENSCAD_FLAGS) -D 'what="body.label"' -o "$@" "$<"
another-%-body-scale.stl: another-%.scad another.scad
	$(OPENSCAD_BIN) $(OPENSCAD_FLAGS) -D 'what="body.scale"' -o "$@" "$<"
another-%-lever.stl: another-%.scad another.scad
	$(OPENSCAD_BIN) $(OPENSCAD_FLAGS) -D 'what="lever"' -o "$@" "$<"
another-%-knob.stl: another-%.scad another.scad
	$(OPENSCAD_BIN) $(OPENSCAD_FLAGS) -D 'what="knob"' -o "$@" "$<"
another-%-knob-label.stl: another-%.scad another.scad
	$(OPENSCAD_BIN) $(OPENSCAD_FLAGS) -D 'what="knob.label"' -o "$@" "$<"
another-%-springpad.stl: another-%.scad another.scad
	$(OPENSCAD_BIN) $(OPENSCAD_FLAGS) -D 'what="springpad"' -o "$@" "$<"

multiswitch: multiswitch.c8r.scad $(addsuffix .stl,$(addprefix multiswitch-embeddest-,2 3 4 5 6))
multiswitch-embeddest-%.stl: multiswitch-embeddest.scad
	$(OPENSCAD_BIN) $(OPENSCAD_FLAGS) -D inputs=$* -o "$@" "$<"
multiswitch-metal-%.stl: multiswitch-metal.scad
	$(OPENSCAD_BIN) $(OPENSCAD_FLAGS) -D inputs=$* -o "$@" "$<"

multiswitch.c8r.scad: multiswitch-c8r.scad threads.scad  pushfit_data.scad pushfittery.scad multiswitch.scad
	@echo "Preprocessing into $@…"
	@( \
	 $(ECHO) "/* DO NOT. JUST DO NOT. FORGET THIS FILE, IT'S PREPROCESSED, GET THE SOURCE. */" ;\
	 $(AWK) '/^\/\/CUSTOMIZERCUTOFF/ {nextfile} {print}' $^ | $(SED) -Ee 's,^(use|include),//\1,g' \
	) > $@

%.stl: %.scad
	${OPENSCAD_BIN} ${OPENSCAD_FLAGS} -o "$@" "$<"
